<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class JobForm extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $url;
    public $subject;

    public function __construct($data, $url)
    {
        $this->subject($this->subject.' - '. $data['adsoyad']);
        $this->data = $data;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('admin.mail.job_form');
    }
}

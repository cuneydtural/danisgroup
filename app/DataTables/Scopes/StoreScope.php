<?php

namespace App\DataTables\Scopes;

use Yajra\Datatables\Contracts\DataTableScopeContract;
use App;

class StoreScope implements DataTableScopeContract
{

    public $locale;

    public function __construct()
    {
        $this->locale = App::getLocale();
    }


    /**
     * Apply a query scope.
     *
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function apply($query)
    {
        switch ($sort = session('sort')) {

            case 1:
                return $query->where('locale', $this->locale)->where('category', 1);
                break;
            case 2:
                return $query->where('locale', $this->locale)->where('category', 2);
                break;
            case 3:
                return $query->where('locale', $this->locale)->where('category', 3);
                break;
            case 'all':
                return $query->where('locale', $this->locale)->orderBy('id', 'desc');
                break;
            default: null;
        }

        if (is_null($sort)) {
            return $query->where('locale', $this->locale)->orderBy('id', 'desc');
        }
    }
}

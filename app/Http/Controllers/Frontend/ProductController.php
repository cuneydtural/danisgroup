<?php       

namespace App\Http\Controllers\Frontend;
                
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    public function index($slug = null)
    {

        if(!is_null($slug)) {
            $index = false;
            $products = Product::whereHas('categories', function($query) use ($slug) {
                $query->whereSlug($slug);
            })->where('locale', $this->locale)->get();
        } else {
            $index = true;
            $products = Product::where('locale', $this->locale)->get();
        }
        $productCategories = Category::where('slug', '=', 'urun-kategorileri')->first()->getDescendants();

        return view('frontend.products.index', compact('products', 'productCategories', 'index'));
    }

    public function show($id)
    {
        $id = intval($id);
        $product = Product::with('photos', 'categories')->where('id', $id)->where('locale', $this->locale)->first();

        if(!$product) {
            return redirect('/');
        }


        $other = Product::with('photos', 'categories')->where('id', '<>', $id)->where('locale', $this->locale)->take(4)->get();
        return view('frontend.products.show', compact('product', 'other'));
    }
}

<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Subscriber;

class SubscriberController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:subscribers',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => 'E-mail adresiniz kaydedilemedi!',
            ]);
        } else {
            $subscriber = Subscriber::create(['email' => $request->input('email'), 'name' => 'Bilinmiyor']);
            if($subscriber) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'E-mail adresiniz kaydedildi!',
                ]);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'E-mail adresiniz kaydedilemedi!',
                ]);
            }
        }
    }
}

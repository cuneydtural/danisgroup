<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;

class ArticleController extends Controller
{

    /**
     * @param $slug
     * @return mixed
     */
    public function getServiceDetail($slug)
    {

        $service = Article::with('photos', 'categories')->where('slug', $slug)->where('locale', $this->locale)->first();

        if(!$service) {
            return redirect('/');
        }

        $nav = Article::with('photos', 'categories')->where(['category' => '27', 'locale' => $this->locale])
            ->where('active', 1)->orderBy('id', 'asc')->get();

        return view('frontend.services.detail', compact('service', 'nav'));
    }

    /**
     * @return mixed
     */
    public function getServiceIndex()
    {
        $services = Article::with('photos', 'categories')->where(['category' => '27', 'locale' => $this->locale])
            ->where('active', 1)->orderBy('id', 'asc')->get();

        return view('frontend.services.index', compact('services'));
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function getPageDetail($slug)
    {
        $article = Article::with('photos', 'categories')->where('slug', $slug)->where('locale', $this->locale)->first();

        if(!$article) {
            return redirect('/');
        }

        $nav = Article::with('photos', 'categories')->where(['category' => 49, 'locale' => $this->locale])
            ->where('active', 1)->orderBy('id', 'asc')->get();

        return view('frontend.page.detail', compact('article', 'nav'));

    }

 }

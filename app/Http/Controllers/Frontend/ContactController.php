<?php

namespace App\Http\Controllers\Frontend;

use App\Mail\ContactForm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Form;
use Validator;
use Mail;

class ContactController extends Controller
{
    public function index()
    {
        return view('frontend.contact.index');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function sendForm(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {

            return response()->json([
                'status' => 'error',
                'message' => 'Lütfen bilgilerini tamamlayın.',
            ]);

        } else {
            try {
                $val = Form::create([
                    'name' => $data['name'].' '.$data['lastname'],
                    'email' => $data['email'],
                    'message' => $data['message'],
                    'phone' => null,
                    'type' => 2,
                ]);
                Mail::to(env('MAIL_TO'))->send(new ContactForm($val));
                return response()->json([
                    'status' => 'success',
                    'message' => 'Mesajınız gönderildi',
                ]);

            } catch (\Exception $e){

                return response()->json([
                    'status' => 'success',
                    'message' => $e->getMessage()
                ]);

            }
        }
    }


}


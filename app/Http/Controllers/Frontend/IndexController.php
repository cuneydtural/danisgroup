<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slide;
use App\Article;
use Mcamara\LaravelLocalization\LaravelLocalization;

class IndexController extends Controller
{
    public function getIndex() {
        $hizmetler = Article::with('photos', 'categories')->where(['category' => '27', 'locale' => $this->locale])
            ->where('active', 1)->orderBy('id', 'asc')->get();

        $duyurular = Article::with('photos', 'categories')->where('locale',$this->locale)->whereIn('category', [57,58])
            ->where('active', 1)->orderBy('id', 'desc')->take(4)->get();

        $slides = Slide::where('locale', $this->locale)->orderBy('list_id','asc')->where('active', 1)->get();

        $referanslar = Article::with('photos', 'categories')->where(['category' => '47', 'locale' => $this->locale])
            ->where('active', 1)->orderBy('id', 'desc')->get();

        $lokasyonlar = Article::with('photos', 'categories')->where(['category' => '59', 'locale' => $this->locale])
            ->where('active', 1)->orderBy('id', 'asc')->first();

        
        return view('frontend.index', compact('slides', 'hizmetler', 'duyurular', 'referanslar', 'lokasyonlar'));
    }

}

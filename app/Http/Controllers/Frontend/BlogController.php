<?php

namespace App\Http\Controllers\Frontend;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;

class BlogController extends Controller
{
    /**
     * Index
     */
    public function index()
    {
        $category = Category::where('slug', '=', 'bizden-haberler')->with('locales')->first();
        $kategoriler = Category::where('slug', '=', 'blog')->first()->children()->orderBy('list_id', 'asc')->with('locales')->get();
        $articles = Article::with('photos', 'categories')->where('locale',$this->locale)->where('category', 57)
            ->where('active', 1)->orderBy('id', 'desc')->paginate(5);
        return view('frontend.blog.index', compact('articles', 'category', 'kategoriler'));
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function show($slug)
    {
        $kategoriler = Category::where('slug', '=', 'blog')->first()->children()->orderBy('list_id', 'asc')->with('locales')->get();
        $others = Article::with('photos', 'categories')->where('locale', $this->locale)->where('category',57)->take(10)->get();
        $article = Article::whereSlug($slug)->first();
        return view('frontend.blog.show', compact('article', 'others', 'kategoriler'));
    }

    public function indexPress()
    {
        $category = Category::where('slug', '=', 'basinda-biz')->with('locales')->first();
        $kategoriler = Category::where('slug', '=', 'blog')->first()->children()->orderBy('list_id', 'asc')->with('locales')->get();
        $articles = Article::with('photos', 'categories')->where('locale',$this->locale)->where('category', 58)
            ->where('active', 1)->orderBy('id', 'desc')->paginate(5);

        return view('frontend.blog.index', compact('articles', 'category', 'kategoriler'));
    }

    public function showPress($slug)
    {
        $kategoriler = Category::where('slug', '=', 'blog')->first()->children()->orderBy('list_id', 'asc')->with('locales')->get();
        $others = Article::with('photos', 'categories')->where('locale', $this->locale)->where('category', 58)->take(10)->get();
        $article = Article::with('categories')->whereSlug($slug)->first();
        return view('frontend.blog.show', compact('article', 'others', 'kategoriler'));
    }

}

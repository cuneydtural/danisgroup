<?php

namespace App\Http\Controllers\Frontend;

use App\Article;
use App\Mail\JobForm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{

    public static $file_extensions = ['pdf', 'doc', '.xls', 'jpg'];


    public function show($slug) {
        $article = Article::where('slug', $slug)->where('locale', $this->locale)->first();
        return view('frontend.page.show', compact('article'));
    }

    public function isbasvuru() {
        $subject = 'İş Başvuru Formu';
        return view('frontend.page.isbasvuru', compact('subject'));
    }

    public function ikpost(Request $request) {

        $subject = 'İş Başvuru Formu';

        $validator = Validator::make($request->all(), [
            'adsoyad' => 'required',
            'tck' => 'required',
            'telefon' => 'required'
        ]);

        if ($validator->fails()) {
            $notify = [
                'status' => 'error',
                'message' => 'Eksik alanlar var !',
            ];
            return view('frontend.page.isbasvuru', compact(['subject','notify']));
        }


        $data = $request->all();

        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $file_extension = $request->file('file')->extension();
            $original_name = explode(".", $request->file('file')->getClientOriginalName());
            $title = (isset($options['title'])) ? str_slug($options['title']) : str_slug($original_name[0]);
            $file_name = $title . '-' . uniqid() . '.' . $file_extension;
            $path = (isset($options['folder'])) ? $options['folder'] : 'ik/';

            if (in_array($file_extension, self::$file_extensions)) {

                $upload_success = $file->move($path, $file_name);

                if ($upload_success) {
                    $url = $path.'/'.$file_name;
                    $notify =  [
                        'status' => 'success',
                        'message' => 'Form iletilmiştir.',
                    ];
                    Mail::to(env('MAIL_TO'))->send(new JobForm($data, $url, $subject));

                } else {
                    $notify = [
                        'status' => 'error',
                        'message' => 'Yükleme sırasında hata oluştu.',
                    ];
                }
            } else {
                $notify = [
                    'status' => 'error',
                    'message' => 'Geçersiz dosya uzantısı gönderdiniz.',
                ];
            }

        } else {
            $notify = [
                'status' => 'error',
                'message' => 'Ekli dosya bulunamadı!',
            ];
        }

        return view('frontend.page.isbasvuru', compact(['subject','notify']));
    }

    public function stajbasvuru()
    {
        $subject = 'Staj Başvuru Formu';
        return view('frontend.page.stajbasvuru', compact('subject'));
    }

    public function stajpost(Request $request)
    {

        $subject = 'Staj Başvuru Formu';

        $validator = Validator::make($request->all(), [
            'adsoyad' => 'required',
            'tck' => 'required',
            'telefon' => 'required'
        ]);

        if ($validator->fails()) {
            $notify = [
                'status' => 'error',
                'message' => 'Eksik alanlar var !',
            ];
            return view('frontend.page.stajbasvuru', compact(['subjet', 'notify']));
        }

        $data = $request->all();

        if ($request->hasFile('file')) {

            $file = $request->file('file');
            $file_extension = $request->file('file')->extension();
            $original_name = explode(".", $request->file('file')->getClientOriginalName());
            $title = (isset($options['title'])) ? str_slug($options['title']) : str_slug($original_name[0]);
            $file_name = $title . '-' . uniqid() . '.' . $file_extension;
            $path = (isset($options['folder'])) ? $options['folder'] : 'ik/';

            if (in_array($file_extension, self::$file_extensions)) {

                $upload_success = $file->move($path, $file_name);

                if ($upload_success) {
                    $url = $path.'/'.$file_name;
                    $notify =  [
                        'status' => 'success',
                        'message' => 'Form iletilmiştir.',
                    ];
                    Mail::to(env('MAIL_TO'))->send(new JobForm($data, $url, $subject));

                } else {
                    $notify = [
                        'status' => 'error',
                        'message' => 'Yükleme sırasında hata oluştu.',
                    ];
                }
            } else {
                $notify = [
                    'status' => 'error',
                    'message' => 'Geçersiz dosya uzantısı gönderdiniz.',
                ];
            }

        } else {
            $notify = [
                'status' => 'error',
                'message' => 'Ekli dosya bulunamadı!',
            ];
        }

        return view('frontend.page.stajbasvuru', compact(['subject','notify']));
    }
}

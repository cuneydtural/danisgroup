<?php

namespace App\Http\Controllers;

use App\Form;
use App\Helpers\DiskStatus;
use App\Helpers\Helper;
use App\Http\Requests;
use Analytics;
use Carbon\Carbon;


class DashboardController extends Controller
{

    public $day;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $callback_forms = Form::where('type',1)->take(20)->get();
        $contact_forms = Form::where('type',2)->take(20)->get();

        $info = [
            'message' => 'Hata',
            'alert' => 'error',
            'tr' => 'Lütfen Google Analytics OAuth2 bilgilerinizi kontrol ediniz.',
            'config_file' => storage_path('app/laravel-google-analytics/service-account-credentials.json'),
        ];

        return view('admin.dashboard_error', compact('callback_forms', 'contact_forms'))->with('info', $info);
    }

    public function show($day)
    {
        $days = ['7', '15', '30', '60'];

        if (!in_array($day, $days)) {
            return redirect()->route('admin.dashboard.index');
        }
        session(['day' => $day]);
        return redirect()->route('admin.dashboard.index');
    }

}

<?php

namespace App\Http\ViewComposers;

use App\Setting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;
use App\Store;
use Illuminate\Support\Facades\App;

class SettingsComposer
{
    protected $settings;
    protected $store;
    protected $stores;

    /**
     * SettingsComposer constructor.
     */
    public function __construct()
    {
        if (Schema::hasTable('settings')) {
            $this->settings = Cache::remember('settings', 10, function () {
                return Setting::where('locale', config('app.locale'))->first();
            });
        }

        $this->store = Store::with('cities', 'districts')->where('locale', App::getLocale())->first();
        $this->stores = Store::with('cities', 'districts')->where('category', '1')->get();
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        if($this->settings) {
            $view->with('settings', $this->settings)->with('sube', $this->store)->with('subeler', $this->stores);
        }
    }
}
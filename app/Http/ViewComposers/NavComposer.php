<?php

namespace App\Http\ViewComposers;
use App\Category;
use Illuminate\Support\Facades\App;
use Illuminate\View\View;
use App\Article;


class NavComposer
{
    protected $categories;
    protected $footer;
    protected $store;


    public function __construct()
    {
        $this->categories = Category::where('slug', '=', 'header-menuler')->first()->children()->orderBy('list_id', 'asc')->with('locales')->get();
        $this->footer = Category::where('slug', '=', 'footer-menuler')->where('active', 1)->first()->children()->with('locales')->get();
        $this->services = Article::with('photos', 'categories')->where(['category' => '27', 'locale' => App::getLocale()])
            ->where('active', 1)->orderBy('id', 'asc')->get();
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('categories', $this->categories->where('active', 1))->with('footer', $this->footer->where('active', 1))->with('services', $this->services)->with('store', $this->store);
    }
}
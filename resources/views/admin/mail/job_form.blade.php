<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>İş Başvuru Formu</title>
</head>
<body>

<table width="500" border="1">
    <tr>
        <td>Ad Soyad</td>
        <td>{{ @$data['adsoyad'] }}</td>
    </tr>
    <tr>
        <td>TCK</td>
        <td>{{ @$data['tck'] }}</td>
    </tr>
    <tr>
        <td>Telefon</td>
        <td>{{ @$data['telefon'] }}</td>
    </tr>
    <tr>
        <td>CV URL</td>
        <td>{{ url($url) }}</td>
    </tr>
    <tr>
        <td>Tarih</td>
        <td>{{ \Carbon\Carbon::now() }}</td>
    </tr>
</table>

</body>
</html>
@extends('frontend.layouts.master')
@section('title', $article->title. ' / '.$settings->title)

@section('container')

    <div class="page-title-style02 pt-bkg02">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>{{ \App\Category::getLocaleCategories($article->categories) }}</h1>
                </div><!-- .col-md-6 end -->

                <div class="col-md-6">
                    <div class="breadcrumb-container">
                        <ul class="breadcrumb clearfix">
                            <li>Buradasınız:</li>
                            <li>
                                <a href="{{ url('/') }}">Anasayfa</a>
                            </li>
                            <li>
                                <a href="#!">{{ \App\Category::getLocaleCategories($article->categories) }}</a>
                            </li>

                            <li>
                                <a href="#">{{ $article->title }}</a>
                            </li>
                        </ul><!-- .breadcrumb end -->
                    </div><!-- .breadcrumb-container end -->
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div style="float:left; width: 100%"><h1>{{ $article->title }}</h1></div>
                    {!!  $article->content !!}
                    <br>
                </div><!-- .col-md-9 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>

@endsection

@section('css')
@endsection
@section('js')
@endsection
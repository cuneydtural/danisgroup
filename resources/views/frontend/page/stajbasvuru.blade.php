@extends('frontend.layouts.master')
@section('title', 'İş Başvuru Formu')

@section('css')
    <style>
        .alert-success {
            background-color: green;
            color:white;
            margin-bottom:30px;
            border-radius:2px;
            padding:20px;
            font-size:20px;
        }

        .alert-error {
            background-color: red;
            color:white;
            margin-bottom:30px;
            border-radius:2px;
            padding:20px;
            font-size:20px;
        }
    </style>
@endsection

@section('container')

    <div class="page-title-style02 pt-bkg02">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>{{ $subject }}</h1>
                </div><!-- .col-md-6 end -->

                <div class="col-md-6">
                    <div class="breadcrumb-container">
                        <ul class="breadcrumb clearfix">
                            <li>Buradasınız:</li>
                            <li>
                                <a href="{{ url('/') }}">Anasayfa</a>
                            </li>
                        </ul><!-- .breadcrumb end -->
                    </div><!-- .breadcrumb-container end -->
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>
    <div class="page-content">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    @if(isset($notify))
                        <div class="alert alert-{{ $notify['status'] }}">
                            {{ $notify['message'] }}
                        </div>
                    @endif
                </div>

                <div class="col-md-12">
                    <!-- .contact form start -->
                        {{ Form::open(['route' => ['frontend.ik.stajbasvuru.post'],
           'class' => 'wpcf7 clearfix', 'autocomplete'=>'off', 'files' => true, 'method' => 'post']) }}

                        <fieldset>
                            <label>@lang('messages.adsoyad')</label>
                            <input type="text" name="adsoyad" class="wpcf7-text" id="adsoyad">
                        </fieldset>

                        <fieldset>
                            <label>@lang('messages.tck')</label>
                            <input type="text" name="tck" class="wpcf7-text" id="tck">
                        </fieldset>

                        <fieldset>
                            <label>@lang('messages.telefon')</label>
                            <input type="text" name="telefon" class="wpcf7-text" id="telefon">
                        </fieldset>

                        <fieldset>
                            <label>@lang('messages.cv_yukle')</label>
                            <input type="file" name="file" class="wpcf7-text" id="file">
                            <small><br>@lang('messages.gecerli_dosya_uzantilari')</small>
                        </fieldset>

                        <input type="submit" class="wpcf7-submit" value="@lang('messages.gonder')" />
                   {{ Form::close() }}
                </div>
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>

@endsection

@section('css')
@endsection
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    <script>
        jQuery(function($){
            $("#telefon").mask("(999) 999-9999");
            $("#tck").mask("99999999999");
        });
    </script>
@endsection
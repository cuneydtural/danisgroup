@extends('frontend.layouts.master')

@section('css')
    <style>
        .adres-list {
            float:left;
            width: 100%;
            margin-bottom:20px;
            border-top:1px solid #CCC;
            padding-top:20px;
        }

        .adres-list .sube {
            font-size:20px;
            color:black;
            margin-bottom:10px;
        }
    </style>
@endsection

@section('container')

    <div class="page-title-style02 pt-bkg02">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>@lang('messages.iletisim')</h1>
                </div><!-- .col-md-6 end -->

                <div class="col-md-6">
                    <div class="breadcrumb-container">
                        <ul class="breadcrumb clearfix">
                            <li>@lang('messages.buradasiniz')</li>
                            <li>
                                <a href="{{ url('/') }}">@lang('messages.anasayfa')</a>
                            </li>
                            <li>
                                <a href="#!">@lang('messages.iletisim')</a>
                            </li>
                        </ul><!-- .breadcrumb end -->
                    </div><!-- .breadcrumb-container end -->
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="custom-heading">
                        <h3>@lang('messages.iletisim_formu')</h3>
                    </div><!-- .custom-heading.left end -->

                    <!-- .contact form start -->

                        {{ Form::open(['route' => ['frontend.contact.post'],'id' => 'request-a-callback', 'onSubmit'=>'return false', 'class' => 'wpcf7 clearfix']) }}

                        <fieldset>
                            <input name="first_name" type="text" class="wpcf7-text" id="contact-name" placeholder="@lang('messages.isim')">
                        </fieldset>

                        <fieldset>
                            <input name="last_name" type="text" class="wpcf7-text" id="contact-last-name" placeholder="@lang('messages.soyisim')">
                        </fieldset>

                        <fieldset>
                            <input name="email" type="email" class="wpcf7-text" id="contact-email" placeholder="@lang('messages.email')">
                        </fieldset>

                        <fieldset>
                            <textarea name="message" rows="8" class="wpcf7-textarea" id="contact-message" placeholder="@lang('messages.mesajiniz')"></textarea>
                        </fieldset>

                        <input type="submit" class="wpcf7-submit" value="@lang('messages.gonder')" />

                        {{ Form::close() }}
                </div><!-- .col-md-6 end -->

                <div class="col-md-6">
                    <div class="custom-heading">
                        <h3>@lang('messages.konum')</h3>
                    </div><!-- .custom-heading end -->

                    <div id="map" style="height: 200px; margin-bottom:30px;"></div>

                    <div class="custom-heading">
                        <h4>@lang('messages.iletisim_bilgileri')</h4>
                    </div><!-- .custom-heading end -->

                    <div class="adres-list">
                        <div class="sube">{{ $sube->name }}</div>
                        <div class="adres">{!! $sube->address.' '.$sube->cities->city.' / '.$sube->districts->district !!}</div>
                        <div class="telefon">@lang('messages.telefon') {{ $sube->phone }} </div>
                        <div class="fax">Fax {{ $sube->fax }}</div>
                        <div class="email"><a href="mailto:{{ $sube->email }}">{{ $sube->email }}</a></div>
                    </div>

                    @foreach($subeler as $sube)
                        <div class="adres-list">
                            <div class="sube">{{ $sube->name }}</div>
                            <div class="adres">{!! $sube->address.' '.$sube->cities->city.' / '.$sube->districts->district !!}</div>
                            <div class="telefon">@lang('messages.telefon') {{ $sube->phone }} </div>
                            <div class="fax">Fax {{ $sube->fax }}</div>
                            <div class="email"><a href="mailto:{{ $sube->email }}">{{ $sube->email }}</a></div>
                        </div>
                    @endforeach

                </div><!-- .col-md-6 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .page-content end -->


@endsection

@section('css')
@endsection
@section('js')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFnz3joDEAxMjMZhjPVQOCER_p0e6H2NI&callback=initMap"></script>
    <script src="/js/jquery.ui.map.full.min.js"></script><!-- google maps -->
    <script src="/js/jquery.dlmenu.min.js"></script><!-- for responsive menu -->
    <script src="/js/include.js"></script><!-- custom js functions -->

    <script>
        function initMap() {
            var uluru = {lat: 41.009585, lng: 28.959746};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map,
                icon: '/img/marker.png'
            });
        }

        $('.wpcf7-submit').on('click', function (event) {
            event.preventDefault();
            var token = $('meta[name="csrf-token"]').attr('content');
            var name = $('#contact-name').val();
            var lastname = $('#contact-last-name').val();
            var email = $('#contact-email').val();
            var contact_message = $('#contact-message').val();
            $.ajax({
                type: 'POST',
                url: "{{ route('frontend.contact.post') }}",
                data: ({'name': name, 'lastname': lastname, 'email':email, 'message': contact_message, '_token': token})
            }).done(function (data) {
                var result = $.parseJSON(JSON.stringify(data));
                console.log(result);
                swal({
                    title: result.message,
                    confirmButtonColor: "#66BB6A",
                    type: result.status,
                }, function () {
                    location.reload();
                });
            });
        });
    </script>
@endsection
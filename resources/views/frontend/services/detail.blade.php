@extends('frontend.layouts.master')
@section('title', $service->title. ' / '.$settings->title)

@section('container')

<div class="page-title-style02 pt-bkg02">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1>@lang('messages.hizmetler')</h1>
            </div><!-- .col-md-6 end -->

            <div class="col-md-6">
                <div class="breadcrumb-container">
                    <ul class="breadcrumb clearfix">
                        <li>@lang('messages.buradasiniz'):</li>
                        <li>
                            <a href="{{ url('/') }}">@lang('messages.anasayfa')</a>
                        </li>
                        <li>
                            <a href="#!">@lang('messages.hizmetler')</a>
                        </li>

                        <li>
                            <a href="#">{{ $service->title }}</a>
                        </li>
                    </ul><!-- .breadcrumb end -->
                </div><!-- .breadcrumb-container end -->
            </div><!-- .col-md-12 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div>
<div class="page-content">
    <div class="container">
        <div class="row">
            <aside class="col-md-3 aside aside-left">
                <ul class="aside-widgets">
                    <li class="widget widget_nav_menu clearfix">
                        <div class="title">
                            <h3>{{ \App\Category::getLocaleCategories($service->categories) }}</h3>
                        </div>

                        <ul class="menu">
                            @foreach($nav as $n)
                            <li class="menu-item @if(Request::segment(3) == $n->slug) current-menu-item @endif">
                                <a href="{{ url(\App\Article::getLocaleCategorySlug($n).'/'.$n->slug) }}">{{ $n->title }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </aside>

            <div class="col-md-9">
                <div style="float:left; width: 100%"><h1>{{ $service->title }}</h1></div>
                <img class="float-right" src="{{ url('/photos/'.@$service->photos[0]->name) }}" alt="" width="360"/>
                {!!  $service->content !!}
            </div><!-- .col-md-9 end -->
        </div><!-- .row end -->
    </div><!-- .container end -->
</div>

@endsection

@section('css')
@endsection
@section('js')
@endsection
@extends('frontend.layouts.master')
@section('title', '')

@section('container')

    <div class="page-title-style02 pt-bkg02">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>@lang('messages.hizmetler')</h1>
                </div><!-- .col-md-6 end -->

                <div class="col-md-6">
                    <div class="breadcrumb-container">
                        <ul class="breadcrumb clearfix">
                            <li>@lang('messages.buradasiniz'):</li>
                            <li>
                                <a href="{{ url('/') }}">@lang('messages.anasayfa')</a>
                            </li>
                            <li>
                                <a href="#">@lang('messages.hizmetler')</a>
                            </li>
                        </ul><!-- .breadcrumb end -->
                    </div><!-- .breadcrumb-container end -->
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>

    <div class="page-content custom-bkg bkg-grey">
        <div class="container">

            <div class="row">
                @foreach($services as $service)
                <div class="col-md-4 col-sm-4">
                    <div class="service-feature-box" style="margin-bottom:30px;">
                        <div class="service-media">
                            <img src="{{ url('photos/'.@$service->photos[0]->name) }}" alt="{{ $service->name }}"/>

                            <a href="{{ url(\App\Article::getLocaleCategorySlug($service).'/'.$service->slug) }}" class="read-more02">
                                    <span>@lang('messages.detay')<i class="fa fa-chevron-right"></i></span>
                            </a>
                        </div><!-- .service-media end -->

                        <div class="service-body">
                            <div class="custom-heading">
                                <h4>{{ $service->title }}</h4>
                            </div><!-- .custom-heading end -->

                            <p>
                                {{ $service->desc }}
                            </p>
                        </div><!-- .service-body end -->
                    </div><!-- .service-feature-box-end -->
                </div>
                @endforeach
            </div><!-- .row end -->

        </div><!-- .container end -->
    </div><!-- .page-content end -->


@endsection

@section('css')
@endsection
@section('js')
@endsection
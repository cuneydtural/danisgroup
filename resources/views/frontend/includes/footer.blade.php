<div id="footer-wrapper" class="footer-dark">
    <footer id="footer">
        <div class="container">
            <div class="row">
                <ul class="col-md-3 col-sm-6 footer-widget-container clearfix">
                    <!-- .widget.widget_text -->
                    <li class="widget widget_newsletterwidget">
                        <div class="title">
                            <h3>@lang('messages.ebulten')</h3>
                        </div>

                        <p>
                            @lang('messages.ebulten_text')
                        </p>

                        <br />

                        <form class="newsletter">
                            <input class="email" type="email" placeholder="E-Mail...">
                            <input type="submit" class="submit" value="">
                        </form>
                    </li><!-- .widget.widget_newsletterwidget end -->
                </ul><!-- .col-md-3.footer-widget-container end -->

                <ul class="col-md-3 col-sm-6 footer-widget-container">
                    <!-- .widget-pages start -->
                    <li class="widget widget_pages">
                        <div class="title">
                            <h3>@lang('messages.site_haritasi')</h3>
                        </div>

                        <ul>
                            {{ \App\Helpers\Frontend::getFooterNav($footer) }}
                        </ul>
                    </li><!-- .widget-pages end -->
                </ul><!-- .col-md-3.footer-widget-container end -->

                <ul class="col-md-3 col-sm-6 footer-widget-container">
                    <!-- .widget-pages start -->
                    <li class="widget widget_pages">
                        <div class="title">
                            <h3>@lang('messages.hizmetler')</h3>
                        </div>
                        <ul>
                            @foreach($services as $service)
                            <li><a href="{{ url(\App\Article::getLocaleCategorySlug($service).'/'.$service->slug) }}">{{ $service->title }}</a></li>
                            @endforeach
                        </ul>
                    </li><!-- .widget-pages end -->
                </ul><!-- .col-md-3.footer-widget-container end -->

                <ul class="col-md-3 col-sm-6 footer-widget-container">
                    <li class="widget widget-text">
                        <div class="title">
                            <h3>@lang('messages.iletisim')</h3>
                        </div>
                        <address>{!! $sube->address.' '.$sube->cities->city.' / '.$sube->districts->district !!}</address>
                        <p>{{ $sube->phone }}</p>
                        <a href="mailto:">{{ $sube->email }}</a>
                        <br /><br /><br />
                        <p>@lang('messages.sosyal_medyada_bizi_takip_edin')</p>
                        <ul class="footer-social-icons">
                            <li><a target="_blank" href="{{ $settings->facebook_url }}" class="fa fa-facebook"></a></li>
                            <li><a target="_blank" href="{{ $settings->twitter_url }}" class="fa fa-twitter"></a></li>
                            <li><a target="_blank" href="{{ $settings->instagram_url }}" class="fa fa-instagram"></a></li>
                        </ul><!-- .footer-social-icons end -->
                    </li><!-- .widget.widget-text end -->
                </ul><!-- .col-md-3.footer-widget-container end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </footer><!-- #footer end -->

    <div class="copyright-container">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p style="text-transform: none">Danıs Group 2017. All Rights Reserved</p>
                </div><!-- .col-md-6 end -->

                <div class="col-md-6">
                    <p class="align-right" style="text-transform: none !important;">Developed by <a href="http://www.bilgeajans.com.tr/">Bilge Ajans</a></p>
                </div><!-- .col-md-6 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .copyright-container end -->

    <a href="#" class="scroll-up">Scroll</a>
</div><!-- #footer-wrapper end -->
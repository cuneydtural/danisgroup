<div class="header-wrapper">
    <!-- .header.header-style01 start -->
    <header id="header"  class="header-style0">
        <!-- .container start -->
        <div class="container-fluid">
            <!-- .main-nav start -->
            <div class="main-nav">
                <!-- .row start -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <nav class="navbar navbar-default nav-left" role="navigation">
                                <!-- .navbar-header start -->
                                <div class="navbar-header">
                                    <div class="logo" style="margin-top:-20px;">
                                        <a href="{{ url('/') }}">
                                            <img src="/img/logo.png" alt=""/>
                                        </a>
                                    </div><!-- .logo end -->
                                </div><!-- .navbar-header start -->

                                <!-- MAIN NAVIGATION -->
                                <div class="collapse navbar-collapse">
                                    {{ \App\Helpers\Frontend::getHeaderNav($categories) }}

                                    <!-- RESPONSIVE MENU -->
                                    <div id="dl-menu" class="dl-menuwrapper">
                                        <button class="dl-trigger">MENÜ</button>
                                        {{ \App\Helpers\Frontend::getResponsiveNav($categories) }}
                                    </div><!-- #dl-menu end -->

                                    <!-- #search start -->

                                    <div id="language" style="width: 70px; z-index:9999; float:right; margin-top:14px;">

                                        <div class="wpml-languages">
                                            <a class="active" href="{{ url(LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale())) }}">
                                                <img src="/img/{{ LaravelLocalization::getCurrentLocale() }}.png">
                                                <i class="fa fa-chevron-down"></i>
                                            </a>
                                            <ul class="wpml-lang-dropdown">
                                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                                    @continue(App::getLocale() == $localeCode)
                                                    <li>
                                                        <a href="{{ url(LaravelLocalization::getLocalizedURL($localeCode)) }}">
                                                            <img src="/img/{{$localeCode}}.png" alt="{{ $properties['native'] }}">
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul><!-- .wpml-lang-dropdown end -->
                                        </div>
                                    </div><!-- #search end -->
                                </div><!-- MAIN NAVIGATION END -->
                            </nav><!-- .navbar.navbar-default end -->
                        </div><!-- .col-md-12 end -->
                    </div><!-- .row end -->
                </div>
            </div><!-- .main-nav end -->
        </div><!-- .container end -->
    </header><!-- .header.header-style01 -->
</div><!-- .header-wrapper end -->
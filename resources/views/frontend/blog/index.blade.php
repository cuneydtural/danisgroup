@extends('frontend.layouts.master')

@section('title', \App\Category::getLocaleCategories($category) .' - '. $settings->title)

@section('container')

    <div class="page-title-style02 pt-bkg02">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>{{ \App\Category::getLocaleCategories($category)  }}</h1>
                </div><!-- .col-md-6 end -->

                <div class="col-md-6">
                    <div class="breadcrumb-container">
                        <ul class="breadcrumb clearfix">
                            <li>@lang('messages.buradasiniz')</li>
                            <li>
                                <a href="{{ url('/') }}">@lang('messages.anasayfa')</a>
                            </li>
                            <li>
                                <a href="#!">{{ \App\Category::getLocaleCategories($category)  }}</a>
                            </li>

                        </ul><!-- .breadcrumb end -->
                    </div><!-- .breadcrumb-container end -->
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>

    <div class="page-content">
        <div class="container">
            <div class="row">
                <ul class="col-md-9 blog-posts post-list">

                    @foreach($articles as $article)
                    <li class="blog-post clearfix">
                        <div class="post-date">
                            <p class="day">{{ $article->created_at->format('d') }}</p>
                            <p class="month">{{ Helper::getShortMount($article->created_at) }}</p>
                        </div><!-- .post-date end -->

                        <div class="post-body">
                            <h3>{{ $article->title }}</h3>

                            <p>
                              {{ $article->desc }}
                            </p>

                            <a href="{{ url(\App\Article::getLocaleCategorySlug($article).'/'.$article->slug) }}" class="read-more">
                                    <span>
                                        Detay
                                        <i class="fa fa-chevron-right"></i>
                                    </span>
                            </a>
                        </div><!-- .post-body end -->
                    </li>
                    @endforeach


                    <li class="pagination clearfix">
                        <ul>
                            <li><a href="{{ $articles->previousPageUrl() }}"><i class="fa fa-arrow-left" aria-hidden="true"></i></a></li>
                            <li><a href="{{ $articles->nextPageUrl() }}"><i class="fa fa-arrow-right" aria-hidden="true"></i></a></li>
                        </ul>
                    </li><!-- .pagination end -->
                </ul><!-- .col-md-9.blog-posts.post-list end -->

                <!-- aside.aside-left start -->
                <aside class="col-md-3 aside aside-left">
                    <!-- .aside-widgets start -->
                    <ul class="aside-widgets">
                        <li class=" widget widget_categories">
                            <div class="title">
                                <h3>@lang('messages.kategoriler')</h3>
                            </div>

                            <ul>
                                @foreach($kategoriler as $kategori)
                                    <li><a href="{{ \App\Category::getCategoryLink($kategori) }}">{{ \App\Category::getLocaleCategories($kategori) }}</a></li>
                                @endforeach
                            </ul>
                        </li><!-- .widget_categories end -->
                    </ul><!-- .aside-widgets end -->
                </aside><!-- .aside.aside-left end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .page-content end -->


@endsection

@section('css')
@endsection
@section('js')
@endsection
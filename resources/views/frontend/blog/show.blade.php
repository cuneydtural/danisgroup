@extends('frontend.layouts.master')

@section('title', $article->title.' - '.\App\Category::getLocaleCategories($article->categories) .' - '. $settings->title)

@section('container')

    <div class="page-title-style02 pt-bkg02">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>{{ \App\Category::getLocaleCategories($article->categories)  }}</h1>
                </div><!-- .col-md-6 end -->

                <div class="col-md-6">
                    <div class="breadcrumb-container">
                        <ul class="breadcrumb clearfix">
                            <li>Buradasınız:</li>
                            <li>
                                <a href="{{ url('/') }}">Anasayfa</a>
                            </li>
                            <li>
                                <a href="#!">{{ \App\Category::getLocaleCategories($article->categories)  }}</a>
                            </li>

                            <li>
                                <a href="#">{{ $article->title }}</a>
                            </li>
                        </ul><!-- .breadcrumb end -->
                    </div><!-- .breadcrumb-container end -->
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-9 blog-posts post-single">
                    <div class="blog-post clearfix">
                        <div class="post-media">
                            <img src="{{ url('photos/'.@$article->photos[0]->name) }}"/>
                        </div><!-- .post-media end -->

                        <div class="post-date">
                            <p class="day">{{ $article->created_at->format('d') }}</p>
                            <p class="month">{{ Helper::getShortMount($article->created_at) }}</p>
                        </div><!-- .post-date end -->

                        <div class="post-body">

                            <h2>{{ $article->title }}</h2>
                            {!! $article->content !!}
                        </div><!-- .post-body end -->
                    </div><!-- .blog-post end -->
                </div><!-- .col-md-9.blog-posts.post-list end -->

                <!-- aside.aside-left start -->
                <aside class="col-md-3 aside aside-left">
                    <!-- .aside-widgets start -->
                    <ul class="aside-widgets">
                        <!-- .widget.widget-search start -->

                        <!-- .widget.widget-categories start -->
                        <li class=" widget widget_categories">
                            <div class="title">
                                <h3>Kategoriler</h3>
                            </div>

                            <ul>
                                @foreach($kategoriler as $kategori)
                                <li><a href="{{ url($kategori->slug) }}">{{ \App\Category::getLocaleCategories($kategori) }}</a></li>
                                @endforeach
                            </ul>
                        </li><!-- .widget_categories end -->

                        <!-- .widget.rpw_posts_widget start -->
                        <li class="widget rpw_posts_widget">
                            <div class="title">
                                <h3>Son Haberler</h3>
                            </div>

                            <ul>
                                @foreach($others as $other)
                                <li>
                                    <a href="{{ url($other->categories->slug.'/'.$other->slug) }}">
                                        <h4>
                                            {{ $other->title }}
                                        </h4>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </li><!-- .rpw_posts_widget end -->
                    </ul><!-- .aside-widgets end -->
                </aside><!-- .aside.aside-left end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>

@endsection

@section('css')
@endsection
@section('js')
@endsection
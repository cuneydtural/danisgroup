@extends('frontend.layouts.master')
@section('title', '')


@section('container')

    <div class="page-title-style02 pt-bkg02">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>@lang('messages.urunler')</h1>
                </div><!-- .col-md-6 end -->

                <div class="col-md-6">
                    <div class="breadcrumb-container">
                        <ul class="breadcrumb clearfix">
                            <li>@lang('messages.buradasiniz'):</li>
                            <li>
                                <a href="{{ url('/') }}">@lang('messages.anasayfa')</a>
                            </li>
                            <li>
                                <a href="{{ url('urunler') }}">@lang('messages.urunler')</a>
                            </li>
                        </ul><!-- .breadcrumb end -->
                    </div><!-- .breadcrumb-container end -->
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div style="float:left; width: 100%"><h1>{{ $product->name }}</h1></div>
                    <img class="float-right" src="{{ url('/photos/'.@$product->photos[0]->name) }}" alt="" width="360"/>
                    {!!  $product->content !!}
                    <br>
                    <a href="{{ url('/urunler') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('messages.urun_listesine_geri_don')</a>
                </div><!-- .col-md-9 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>
@endsection

@section('css')
@endsection
@section('js')
@endsection
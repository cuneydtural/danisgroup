@extends('frontend.layouts.master')
@section('title', '')

@section('container')

    <div class="page-title-style02 pt-bkg02">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>@lang('messages.urunler') @if(!$index) / {{ @$products->first()->categories->name }} @endif</h1>
                </div><!-- .col-md-6 end -->

                <div class="col-md-6">
                    <div class="breadcrumb-container">
                        <ul class="breadcrumb clearfix">
                            <li>@lang('messages.buradasiniz'):</li>
                            <li>
                                <a href="{{ url('/') }}">@lang('messages.anasayfa')</a>
                            </li>
                            <li>
                                <a href="{{ url('urunler') }}">@lang('messages.urunler')</a>
                            </li>
                            <li>
                                @if(!$index) {{ @$products->first()->categories->name }} @endif
                            </li>

                        </ul><!-- .breadcrumb end -->
                    </div><!-- .breadcrumb-container end -->
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>

    <div class="page-content">
        <div class="container">
            <div class="row">
                <ul class="vehicle-gallery clearfix">
                    @foreach($products as $product)
                    <li class="col-md-4">
                        <figure class="gallery-item-container">
                            <div class="gallery-item">
                                <a href="{{ url('urunler/detay/'.@$product->id.'-'.$product->categories->slug.'-'.str_slug($product->name)) }}">
                                <img src="{{ url('photos/'.@$product->photos[0]->name) }}" alt=""/>
                                <div class="hover-mask-container">
                                    <div class="hover-mask"></div>
                                </div><!-- .hover-mask-container end -->
                                </a>
                            </div><!-- .service-item end -->

                            <figcaption>
                                <h3>{{ $product->name }}</h3>
                            </figcaption>
                        </figure><!-- .gallery-item-container end -->
                    </li>
                    @endforeach
                </ul><!-- #vehicle-gallery end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>

@endsection

@section('css')
@endsection
@section('js')
@endsection
@extends('frontend.layouts.master')
@section('container')
    <div id="masterslider" class="master-slider ms-skin-default mb-0">
        @foreach($slides as $slide)
        <div class="ms-slide">
            <!-- slide background -->
            <img src="masterslider/blank.gif" data-src="{{ url('/photos/'.$slide->photo) }}"/>

            <h2 class="ms-layer pi-caption01"
                style="left: 0px; top: 390px; line-height: 50px; font-size:50px; text-transform: none"
                data-type="text"
                data-effect="top(short)"
                data-hide-effect="fade"
                data-delay="00">
                <span style=" text-shadow: 2px 2px #000000; color:white;"> {{ $slide->title }}</span>
            </h2>

            <p class="ms-layer pi-text"
               style="left: 0px; top: 470px;"
               data-type="text"
               data-effect="top(short)"
               data-hide-effect="fade"
               data-delay="600">
                <span style=" text-shadow: 2px 2px #000000; color:white;"> {{ $slide->desc }}</span>
            </p>
        </div>
        @endforeach
    </div><!-- #masterslider end -->

    <div class="page-content parallax parallax01 mb-70">
        <div class="container">
            <div class="row services-negative-top">
                @foreach($hizmetler->slice(0,4) as $hizmet)
                <div class="col-md-3 col-sm-4">
                    <div class="service-feature-box">
                        <div class="service-media">
                            <img src="{{ url('photos/'.$hizmet->photos[0]->name) }}" alt="Trucking"/>

                            <a href="{{ url(\App\Article::getLocaleCategorySlug($hizmet).'/'.$hizmet->slug) }}" class="read-more02">
                                    <span>@lang('messages.detay')<i class="fa fa-chevron-right"></i></span>
                            </a>
                        </div><!-- .service-media end -->

                        <div class="service-body">
                            <div class="custom-heading">
                                <h4>{{ $hizmet->title }}</h4>
                            </div><!-- .custom-heading end -->
                            <p>{{ $hizmet->desc }}</p>
                        </div><!-- .service-body end -->
                    </div><!-- .service-feature-box-end -->
                </div>
                @endforeach
            </div><!-- .row end -->

            <div class="row">
                <div class="col-md-12">
                    <a href="hizmetler" class="btn btn-big btn-yellow btn-centered">
                            <span>
                               @lang('messages.tumunu_goruntule')
                            </span>
                    </a>
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .page-content end -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="custom-heading02">
                        <h2>@lang('messages.nasil_yardimci_olabiliriz')</h2>
                        <p>
                        </p>
                    </div><!-- .custom-heading02 end -->
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->

            <div class="row mb-30">
                <div class="col-md-6 col-sm-6">
                    <div class="service-icon-left-boxed">
                        <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                            <img src="img/svg/pi-checklist-2.svg" alt="checklist icon"/>
                        </div><!-- .icon-container end -->

                        <div class="service-details">
                            <h3>@lang('messages.servis_detay_title_1')</h3>
                            <p>@lang('messages.servis_detay_text_1')</p>
                        </div><!-- .service-details end -->
                    </div><!-- .service-icon-left-boxed end -->
                </div><!-- .col-md-6 end -->

                <div class="col-md-6 col-sm-6">
                    <div class="service-icon-left-boxed">
                        <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                            <img src="img/svg/pi-globe-5.svg" alt="globe icon"/>
                        </div><!-- .icon-container end -->

                        <div class="service-details">
                            <h3>@lang('messages.servis_detay_title_2')</h3>

                            <p>
                                @lang('messages.servis_detay_text_2')
                            </p>
                        </div><!-- .service-details end -->
                    </div><!-- .service-icon-left-boxed end -->
                </div><!-- .col-md-6 end -->
            </div><!-- .row.mb-30 end -->

            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="service-icon-left-boxed">
                        <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                            <img src="img/svg/pi-forklift-truck-5.svg" alt="forktruck icon"/>
                        </div><!-- .icon-container end -->

                        <div class="service-details">
                            <h3>@lang('messages.servis_detay_title_3')</h3>

                            <p>
                                @lang('messages.servis_detay_text_3')
                            </p>
                        </div><!-- .service-details end -->
                    </div><!-- .service-icon-left-boxed end -->
                </div><!-- .col-md-6 end -->

                <div class="col-md-6 col-sm-6">
                    <div class="service-icon-left-boxed">
                        <div class="icon-container animated triggerAnimation" data-animate="zoomIn">
                            <img src="img/svg/pi-touch-desktop.svg" alt="touch icon"/>
                        </div><!-- .icon-container end -->

                        <div class="service-details">
                            <h3>@lang('messages.servis_detay_title_4')</h3>

                            <p>
                                @lang('messages.servis_detay_text_4')
                            </p>
                        </div><!-- .service-details end -->
                    </div><!-- .service-icon-left-boxed end -->
                </div><!-- .col-md-6 end -->
            </div><!-- .row.mb-30 end -->
        </div><!-- .container end -->
    </div><!-- .page-content end -->



    <div class="page-content custom-bkg bkg-dark-blue column-img-bkg dark mb-70">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-md-offset-2 custom-col-padding-both">
                    <div class="custom-heading">
                        <h3>@lang('messages.endustri_sektorleri_kapsami')</h3>
                    </div><!-- .custom-heading end -->

                    <p>
                        @lang('messages.endustri_sektorleri_kapsami_text')
                    </p>

                    <ul class="service-list clearfix">
                        <li>
                            <div class="icon-container">
                                <img class="svg-white" src="img/svg/pi-cargo-box-2.svg" alt="icon"/>
                            </div><!-- .icon-container end -->

                            <p>
                               @lang('messages.endustri_sektorleri_kapsami_madde_1')
                            </p>
                        </li>

                        <li>
                            <div class="icon-container">
                                <img class="svg-white" src="img/svg/pi-mark-energy.svg" alt="icon"/>
                            </div><!-- .icon-container end -->

                            <p>
                                @lang('messages.endustri_sektorleri_kapsami_madde_2')
                            </p>
                        </li>

                        <li>
                            <div class="icon-container">
                                <img class="svg-white" src="img/svg/pi-food-beverage.svg" alt="icon"/>
                            </div><!-- .icon-container end -->

                            <p>
                                @lang('messages.endustri_sektorleri_kapsami_madde_3')

                            </p>
                        </li>

                        <li>
                            <div class="icon-container">
                                <img class="svg-white" src="img/svg/pi-cargo-retail.svg" alt="icon"/>
                            </div><!-- .icon-container end -->

                            <p>
                                @lang('messages.endustri_sektorleri_kapsami_madde_4')
                            </p>
                        </li>

                        <li>
                            <div class="icon-container">
                                <img class="svg-white" src="img/svg/pi-truck-8.svg" alt="icon"/>
                            </div><!-- .icon-container end -->

                            <p>
                               @lang('messages.endustri_sektorleri_kapsami_madde_5')
                            </p>
                        </li>
                    </ul><!-- .service-list end -->
                </div><!-- .col-md-6 end -->

                <div class="col-md-6 img-bkg01">
                </div>
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .page-content.bkg-dark-blue end -->

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="custom-heading">
                        <h3>@lang('messages.duyurular')</h3>
                    </div><!-- .custom-heading end -->

                    <ul class="pi-latest-posts clearfix">
                        @foreach($duyurular->slice(0,1) as $duyuru)
                        <li>
                            <div class="post-media">
                                <img src="{{ url('photos/thumbs/'.@$duyuru->photos[0]->name) }}" alt=""/>
                            </div><!-- .post-media end -->

                            <div class="post-details">
                                <div class="post-date">
                                    <p>
                                        <i class="fa fa-calendar"></i>
                                        {{ \App\Helpers\Helper::diffForHumans($duyuru->created_at) }}
                                    </p>
                                </div>

                                <a href="{{ url($duyuru->categories->slug.'/'.$duyuru->slug) }}">
                                    <h4>
                                        {{ $duyuru->title }}
                                    </h4>
                                </a>

                                <a href="{{ url('haberler/'.$duyuru->slug) }}" class="read-more"><span>Detay<i class="fa fa-chevron-right"></i></span>
                                </a>
                            </div><!-- .post-details end -->
                        </li>
                        @endforeach

                    </ul><!-- .pi-latest-posts end -->
                </div><!-- .col-md-4 end -->
                <div class="col-md-4 col-sm-6">

                    <ul class="pi-latest-posts clearfix" style="margin-top:60px;">
                        @foreach($duyurular->slice(1,1) as $duyuru)
                            <li>
                                <div class="post-media">
                                    <img src="{{ url('photos/thumbs/'.@$duyuru->photos[0]->name) }}" alt=""/>
                                </div><!-- .post-media end -->

                                <div class="post-details">
                                    <div class="post-date">
                                        <p>
                                            <i class="fa fa-calendar"></i>
                                            {{ \App\Helpers\Helper::diffForHumans($duyuru->created_at) }}
                                        </p>
                                    </div>

                                    <a href="{{ url($duyuru->categories->slug.'/'.$duyuru->slug) }}">
                                        <h4>
                                            {{ $duyuru->title }}
                                        </h4>
                                    </a>

                                    <a href="{{ url($duyuru->categories->slug.'/'.$duyuru->slug) }}" class="read-more"><span>Detay<i class="fa fa-chevron-right"></i></span>
                                    </a>
                                </div><!-- .post-details end -->
                            </li>
                        @endforeach

                    </ul><!-- .pi-latest-posts end -->
                </div><!-- .col-md-4 end -->
                <div class="col-md-4 col-sm-6">

                    <ul class="pi-latest-posts clearfix" style="margin-top:60px;">
                        @foreach($duyurular->slice(2,1) as $duyuru)
                            <li>
                                <div class="post-media">
                                    <img src="{{ url('photos/thumbs/'.@$duyuru->photos[0]->name) }}" alt=""/>
                                </div><!-- .post-media end -->

                                <div class="post-details">
                                    <div class="post-date">
                                        <p>
                                            <i class="fa fa-calendar"></i>
                                            {{ \App\Helpers\Helper::diffForHumans($duyuru->created_at) }}
                                        </p>
                                    </div>

                                    <a href="{{ url($duyuru->categories->slug.'/'.$duyuru->slug) }}">
                                        <h4>
                                            {{ $duyuru->title }}
                                        </h4>
                                    </a>

                                    <a href="{{ url($duyuru->categories->slug.'/'.$duyuru->slug) }}" class="read-more"><span>Detay<i class="fa fa-chevron-right"></i></span>
                                    </a>
                                </div><!-- .post-details end -->
                            </li>
                        @endforeach

                    </ul><!-- .pi-latest-posts end -->
                </div><!-- .col-md-4 end -->

            </div><!-- .row end -->
        </div><!-- .container end -->
    </div><!-- .page-content end -->

    @if(count($referanslar))
    <div class="page-content custom-bkg bkg-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="custom-heading">
                        <h2>Çözüm Ortaklarımız</h2>
                    </div><!-- .custom-heading02 end -->
                    <div class="carousel-container">
                        <div id="client-carousel" class="owl-carousel owl-carousel-navigation">
                            @foreach($referanslar as $referans)
                            <div class="owl-item"><img src="{{ url('photos/'.$referans->photos[0]->name) }}" alt="{{ $referans->title }}"/></div>
                            @endforeach
                        </div><!-- .owl-carousel.owl-carousel-navigation end -->
                    </div><!-- .carousel-container end -->
                </div><!-- .col-md-12 end -->
            </div><!-- .row end -->
        </div><!-- .container end -->
    </div>
    @endif
@endsection

@section('css')
@section('js')
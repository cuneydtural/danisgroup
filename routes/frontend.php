<?php

Route::get('/', function() {
    return view('frontend.index');
});

// Forbidden
Route::get('/', ['as' => 'frontend.index',
    'uses' => 'Frontend\IndexController@getIndex']);

Route::get('/services/', ['as' => 'frontend.service.index',
    'uses' => 'Frontend\ArticleController@getServiceIndex']);

Route::get('/hizmetler/', ['as' => 'frontend.service.index',
    'uses' => 'Frontend\ArticleController@getServiceIndex']);

Route::get('/hizmetler/{slug}', ['as' => 'frontend.service.detail',
    'uses' => 'Frontend\ArticleController@getServiceDetail']);

Route::get('/services/{slug}', ['as' => 'frontend.service.detail',
    'uses' => 'Frontend\ArticleController@getServiceDetail']);

Route::get('/kurumsal/{slug}', ['as' => 'frontend.page.detail',
    'uses' => 'Frontend\ArticleController@getPageDetail']);

Route::get('/corporative/{slug}', ['as' => 'frontend.page.detail',
    'uses' => 'Frontend\ArticleController@getPageDetail']);

Route::get('/products/', ['as' => 'frontend.product.index',
    'uses' => 'Frontend\ProductController@index']);

Route::get('/urunler/{category?}', ['as' => 'frontend.product.category.detail',
    'uses' => 'Frontend\ProductController@index']);

Route::get('/urunler/detay/{slug}', ['as' => 'frontend.products.show',
    'uses' => 'Frontend\ProductController@show']);

Route::get('/our-news', ['as' => 'frontend.blog.index',
    'uses' => 'Frontend\BlogController@index']);

Route::get('/bizden-haberler', ['as' => 'frontend.blog.index',
    'uses' => 'Frontend\BlogController@index']);

Route::get('/our-news/{id}', ['as' => 'frontend.blog.show',
    'uses' => 'Frontend\BlogController@show']);

Route::get('/haberler/{id}', ['as' => 'frontend.blog.show',
    'uses' => 'Frontend\BlogController@show']);

Route::get('/bizden-haberler/{id}', ['as' => 'frontend.blog.show',
    'uses' => 'Frontend\BlogController@show']);

Route::get('/press', ['as' => 'frontend.blog.press.index',
    'uses' => 'Frontend\BlogController@indexPress']);

Route::get('/basinda-biz', ['as' => 'frontend.blog.press.index',
    'uses' => 'Frontend\BlogController@indexPress']);

Route::get('/press/{id}', ['as' => 'frontend.blog.show.press',
    'uses' => 'Frontend\BlogController@showPress']);

Route::get('/basinda-biz/{id}', ['as' => 'frontend.blog.show.press',
    'uses' => 'Frontend\BlogController@showPress']);

Route::get('/urunler/', ['as' => 'frontend.product.index',
    'uses' => 'Frontend\ProductController@index']);

Route::get('/contact/', ['as' => 'frontend.contact.index',
    'uses' => 'Frontend\ContactController@index']);

Route::get('/iletisim/', ['as' => 'frontend.contact.index',
    'uses' => 'Frontend\ContactController@index']);

Route::post('/send-contact-form/', ['as' => 'frontend.contact.post',
    'uses' => 'Frontend\ContactController@sendForm']);

Route::post('/abone/ekle', [
    'uses' => 'Frontend\SubscriberController@store', 'as' => 'frontend.subscriber.store'
]);

Route::get('/sayfalar/{slug}', ['as' => 'frontend.page.show',
    'uses' => 'Frontend\PageController@show']);

Route::get('/is-basvuru-formu', ['as' => 'frontend.ik.isbasvuru',
    'uses' => 'Frontend\PageController@isbasvuru']);

Route::post('/is-basvuru-formu', ['as' => 'frontend.ik.post',
    'uses' => 'Frontend\PageController@ikpost']);

Route::get('/staj-basvuru-formu', ['as' => 'frontend.ik.stajbasvuru',
    'uses' => 'Frontend\PageController@stajbasvuru']);

Route::post('/staj-basvuru-formu', ['as' => 'frontend.ik.stajbasvuru.post',
    'uses' => 'Frontend\PageController@stajpost']);













